import marshmallow as ma
from flask_restful import fields, reqparse

product_fields = {
    'id': fields.String,
    'name': fields.String,
    'creation_date': fields.String
}

product_parser = reqparse.RequestParser()
product_parser.add_argument('name', type=str, help="test title", required=True)


class ProductSchema(ma.Schema):
    id = ma.fields.Integer(required=False)
    name = ma.fields.String(required=True)
    creation_date = ma.fields.DateTime(required=False)


product_list_schema = ProductSchema(many=True)
product_schema = ProductSchema(many=False)
