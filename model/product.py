from datetime import datetime

from mongoengine import  errors
from mongoengine import IntField, StringField, DateTimeField

from app import db


class Product(db.Document):
    """ User Model for storing user related details """

    id = IntField(primary_key=True,
                  null=False,
                  required=False)

    name = StringField(max_length=50,
                       null=False,
                       required=True
                       )
    creation_date = DateTimeField(null=False,
                                  default=datetime.now(),
                                  required=False)

    meta = {'strict': False}

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    @staticmethod
    def create_product(product_schema):
        product = Product()
        product.name = product_schema['name']
        product.id = Product.objects.count() + 1
        try:
            product.save()
            return product
        except errors.NotUniqueError as exc:
            for field in Product._fields:  # You could also loop in User._fields to make something generic
                if field in str(exc):
                    raise Exception('field {} is not unique'.format(field))
