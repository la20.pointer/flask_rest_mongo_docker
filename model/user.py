from flask_jwt_extended import (
    create_access_token, create_refresh_token, get_jti,
    get_jwt_identity)
from mongoengine import Document, errors
from mongoengine import IntField, EmailField, StringField
import app

class UserAlreadyExistsException(Exception):
    pass

class User(Document):

    id = IntField(primary_key=True,
                  null=False,
                  required=True)

    email = EmailField(unique=True,
                       max_length=50,
                       null=False,
                       required=True
                       )

    password_hash = StringField(max_length=1024, unique=False)

    meta = {'strict': False}

    def __unicode__(self):
        return self.email

    def __repr__(self):
        return self.email

    def __str__(self):
        return self.email

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = app.flask_bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return app.flask_bcrypt.check_password_hash(self.password_hash, password)

    @staticmethod
    def create_user(**kwargs):
        password = kwargs.pop('password')
        user = User(**kwargs)
        user.password = password
        user.id = User.objects.count() + 1
        try:
            user.save()
            return user
        except errors.NotUniqueError as exc:
            for field in User._fields:  # You could also loop in User._fields to make something generic
                if field in str(exc):
                    raise UserAlreadyExistsException('field {} is not unique'.format(field))
        except Exception as e:
            print(e)

    def generate_jwt_token(cls):
        access_token = create_access_token(identity=cls.email)
        refresh_token = create_refresh_token(identity=cls.email)

        access_jti = get_jti(encoded_token=access_token)
        refresh_jti = get_jti(encoded_token=refresh_token)
        app.revoked_store.set(access_jti, 'false', app.app.config['ACCESS_EXPIRES'] * 1.2)
        app.revoked_store.set(refresh_jti, 'false', app.app.config['REFRESH_EXPIRES'] * 1.2)
        return {
            'access_token': access_token,
            'refresh_token': refresh_token
        }

    # except Exception as e:
    #     return e

    @staticmethod
    def refresh_auth_token():
        current_user_email = get_jwt_identity()

        access_token = create_access_token(identity=current_user_email)
        access_jti = get_jti(encoded_token=access_token)
        app.revoked_store.set(access_jti, 'false', app.app.config['ACCESS_EXPIRES'] * 1.2)
        return {'access_token': access_token}
