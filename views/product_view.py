from functools import wraps

from flask import jsonify, request, Blueprint
from flask_jwt_extended import jwt_required
from flask_restful import Resource, marshal_with, abort

from app import app, api
from model.product import Product
from schema.product_schema import product_list_schema, product_schema, product_fields, product_parser

product_urls = Blueprint('product_urls', __name__, url_prefix='',
                         template_folder='templates')


@product_urls.route('/product', methods=['POST'])
@jwt_required
def create_product():
    schema = product_schema.load(request.get_json())

    if schema.errors is not None and len(schema.errors) > 0:
        return jsonify(schema.errors), 400

    product = Product.create_product(schema)
    data, errors = product_schema.dump(product)
    return jsonify(data), 201


@product_urls.route('/product', methods=['GET'])
@jwt_required
def products_list():
    products = Product.objects.all()
    data, errors = product_list_schema.dump(products)
    return jsonify(data)


@product_urls.route('/product/<int:id>', methods=['GET'])
@jwt_required
def product_retrieve(id=None):
    product = Product.objects.filter(id=id).first()
    data, errors = product_schema.dump(product)
    return jsonify(data)


class Paginate(object):

    def __call__(self, f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            page = request.args.get('page', 1)
            resp = f(*args, **kwargs).paginate(int(page), 10).items
            return resp

        return wrapper


class ProductListApi(Resource):
    @marshal_with(product_fields, envelope="result")
    @Paginate()
    def get(self):
        return Product.objects.all()

    @marshal_with(product_fields, envelope="result")
    def post(self):
        args = product_parser.parse_args()
        product = Product.create_product(args)
        return product


class ProductDetailApi(Resource):

    @marshal_with(product_fields, envelope="result")
    def get(self, id=None):
        if id is not None:
            product = Product.objects.filter(id=id).first()
            if product is None:
                abort(404)
            return product
        else:
            abort(404)


# app.register_blueprint(product_urls)
api.add_resource(ProductListApi, '/product/')
api.add_resource(ProductDetailApi, '/product/<int:id>/')
