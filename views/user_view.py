from flask import request, jsonify, Blueprint
from flask_jwt_extended import (
    jwt_refresh_token_required, jwt_required, get_raw_jwt
)

import app
from model.user import User, UserAlreadyExistsException

account_urls = Blueprint('account_urls', __name__, url_prefix='',
                         template_folder='templates')


@account_urls.route('/auth/login', methods=['POST'])
def login():
    if request.json is None:
        return jsonify({"msg": "Bad email or password"}), 400

    email = request.json.get('email', None)
    password = request.json.get('password', None)
    if email is None or password is None:
        return jsonify({"msg": "Bad email or password"}), 401

    user = User.objects.filter(email=email ).first()
    if not user:
        if not user.check_password(password):
            return jsonify({"msg": "Bad email or password"}), 401

    response = user.generate_jwt_token()

    return jsonify(response), 201


@account_urls.route('/auth/register', methods=['POST', ])
def register():
    if request.json is None:
        return jsonify({"msg": "Bad email or password"}), 400

    email = request.json.get('email', None)
    password = request.json.get('password', None)
    if email is None or password is None:
        return jsonify({"msg": "Bad email or password"}), 401
    try:
        user = User.create_user(email=email, password=password)
    except UserAlreadyExistsException as e:
        return jsonify({"msg": "user already exists with this email"}), 400

    if user is None:
        return jsonify({"msg": "Bad email or password"}), 400

    response = user.generate_jwt_token()

    return jsonify(response), 201


@account_urls.route('/auth/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    response = User.refresh_auth_token()
    return jsonify(response), 201


@account_urls.route('/auth/access_revoke', methods=['DELETE'])
@jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    app.revoked_store.set(jti, 'true', app.app.config['ACCESS_EXPIRES'] * 1.2)
    return jsonify({"msg": "Access token revoked"}), 200


@account_urls.route('/auth/refresh_revoke', methods=['DELETE'])
@jwt_refresh_token_required
def logout_refresh_token():
    jti = get_raw_jwt()['jti']
    app.revoked_store.set(jti, 'true', app.app.config['REFRESH_EXPIRES'] * 1.2)
    return jsonify({"msg": "Refresh token revoked"}), 200


app.app.register_blueprint(account_urls)
