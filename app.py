import logging
from datetime import timedelta

import redis
from flask import Flask, jsonify, Blueprint
from flask_bcrypt import Bcrypt
from flask_jwt_extended import (
    JWTManager, jwt_required
)
from flask_mongoengine import MongoEngine
from flask_restful import Api

app = Flask(__name__)
app.secret_key = 'ChangeMe!'
app.debug = True
api = Api(app)


class ConfigClass(object):
    """ Flask application config """

    # Flask settings
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'
    # Flask-MongoEngine settings
    MONGODB_SETTINGS = {
        'db': 'flask_rest_login_jwt',
        'host': 'mongodb://127.0.0.1:27017/flask_rest_login_jwt',
        'username': 'root',
        'password': 'root'
    }
    ACCESS_EXPIRES = timedelta(minutes=15)
    REFRESH_EXPIRES = timedelta(days=30)
    JWT_ACCESS_TOKEN_EXPIRES = ACCESS_EXPIRES
    JWT_REFRESH_TOKEN_EXPIRES = REFRESH_EXPIRES
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']


# Setup the flask-jwt-extended extension. See:

jwt = JWTManager(app)

flask_bcrypt = Bcrypt()
key = app.secret_key

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logging.getLogger().setLevel(logging.DEBUG)

# Setup our redis connection for storing the blacklisted tokens
revoked_store = redis.StrictRedis(host='localhost', port=6379, db=0,
                                  decode_responses=True)

app.config.from_object(ConfigClass)

db = MongoEngine(app)

from views import product_view, user_view


@jwt.token_in_blacklist_loader
def check_if_token_is_revoked(decrypted_token):
    jti = decrypted_token['jti']
    entry = revoked_store.get(jti)
    if entry is None:
        return True
    return entry == 'true'


@app.route('/', methods=['GET', ])
def index():
    return 'Home'


if __name__ == '__main__':
    app.run(debug=app.debug)
    db.init_app(app)
